# pandas_DataFrame Application
Application uses pandas library and works with CSV files. It has different abilities, like: 
a) create Series by using different ways [numpy arrays, lists, dictionaries, scalar values, csv file columns] 
b) display and filter subsets from DataFrame [filter with value, select specific rows and columns, sort and display distinct values] 
c) calculate summary statistics based on csv file [mean, median, min, max, skew, standard deviation, row count, value_counts] 
d) manipulate textual data [show values in upper or lower case, split and add columns based on some symbol, find rows that contain concrete values, replace values and e.t.c]

# App:
![1](/uploads/dcfdd61054d278e80fae30eaf1f68d15/1.jpg)
![2](/uploads/ef6a87c01d7c92d161bcc0a8f4a270b6/2.jpg)
![3](/uploads/385db17e85ee9df0194089a17d4c4172/3.jpg)
![4](/uploads/3b869b9acf259c22fd083ec14a4b8438/4.jpg)
![5](/uploads/e0de765af3811e3571d07f8b70081ccc/5.jpg)
